﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using SimpleJSON;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class WXScrollMgr : MonoBehaviour, IEndDragHandler, IBeginDragHandler
{
    public Scrollbar scrollBar;


    public GameObject textPrefab;
    public GameObject timePrefab;
    public GameObject tupianPrefab;
    public GameObject selfPrefab;

    public GameObject contentParant;
    public Sprite npcNameSprite = null;
    public Sprite npcHead = null;
    public Button buttonLeft;
    public Button buttonRight;
    public GameObject chooseObject;
    public GameObject inputIngObject;
    public GameObject busyObject;
    public GameObject baseParentObject;

    private bool keepBottom = true;
    private Sprite playerHead;
    private int msgCount = 0;
    private string playerName;
    private bool isShow = false;
    private List<GameObject> createObjList = new List<GameObject>();
    internal int GetUnReadMsgCount()
    {
        return msgCount;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (!isShow)
            return;
        if(keepBottom)
            scrollBar.value = 0;
    }

    public bool isViewHistory() {
        return !keepBottom;
    }

    public void AddNewMsg(string data, bool isNewMsg) {
        bool flag = isViewHistory();
        string addName = string.Format(data, playerName);
        GameObject obj = (GameObject)GameObject.Instantiate(textPrefab);
        createObjList.Add(obj);
        obj.GetComponentInChildren<AdapatableText>().InputStr(addName, npcHead, npcNameSprite);
        obj.transform.SetParent(contentParant.transform);

        if ((flag || !isShow) && isNewMsg)
            msgCount++;
    }

    public void AddTimeMsg(string data, bool isNewMsg)
    {
        GameObject obj = (GameObject)GameObject.Instantiate(timePrefab);
        createObjList.Add(obj);
        obj.GetComponentInChildren<AdapatableText>().InputStr(data);
        obj.transform.SetParent(contentParant.transform);
    }

    public void AddImgagMsg(string data, bool isNewMsg)
    {
        bool flag = isViewHistory();
        GameObject obj = (GameObject)GameObject.Instantiate(tupianPrefab);
        createObjList.Add(obj);
        obj.GetComponentInChildren<PictureMgr>().SetTuPian(data, npcHead, npcNameSprite);
        obj.transform.SetParent(contentParant.transform);
        if ((flag || !isShow) && isNewMsg)
            msgCount++;
    }

    public void AddSelfMsg(string data)
    {
        inputIngObject.SetActive(false);
        GameObject obj = (GameObject)GameObject.Instantiate(selfPrefab);
        createObjList.Add(obj);
        obj.GetComponentInChildren<AdapatableText>().InputStr(data, playerHead, null);
        obj.transform.SetParent(contentParant.transform);
    }

    internal void Init(string playName, Sprite headImg)
    {
        playerHead = headImg;
        playerName = playName;
    }

    internal void AddChooseMsg(string anniu1, string anniu2)
    {
        inputIngObject.SetActive(false);
        buttonLeft.GetComponentInChildren<Text>().text = anniu1;
        buttonRight.GetComponentInChildren<Text>().text = anniu2;
        chooseObject.SetActive(true);
    }

    internal void AddTextMsg(string str, string time, bool isNewMsg)
    {
        inputIngObject.SetActive(false);
        if (time != null)
            AddTimeMsg(time, isNewMsg);
        AddNewMsg(str, isNewMsg);
    }

    internal void AddImgagMsg(string image, string time, bool isNewMsg)
    {
        inputIngObject.SetActive(false);
        if (time != null)
            AddTimeMsg(time, isNewMsg);
        AddImgagMsg(image, isNewMsg);
    }

    internal void AddWaitingFlag()
    {
        inputIngObject.SetActive(true);
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        keepBottom = scrollBar.value <= 0;
        if (keepBottom)
            msgCount = 0;
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        keepBottom = false;
    }

    public void OnMakeChooseL() {
        StoryMgr.GetInstance().MakeChoose(0);
        chooseObject.SetActive(false);
    }

    public void OnMakeChooseR()
    {
        StoryMgr.GetInstance().MakeChoose(1);
        chooseObject.SetActive(false);
    }

    public void setBusyFlag(bool flag) {
        busyObject.SetActive(flag);
    }

    public void setActived(bool flag) {
        baseParentObject.SetActive(flag);
        isShow = flag;
        if (isShow)
            msgCount = 0;
    }

    public void ClearAllMsg() {
        for (int i = 0; i < createObjList.Count; i++) {
            Destroy(createObjList[i]);
        }
        createObjList.Clear();
    }
}
