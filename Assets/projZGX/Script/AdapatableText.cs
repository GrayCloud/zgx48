﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AdapatableText : MonoBehaviour {
    public int testLeft;
    public int testRight;
    public int testTop;
    public int testBottom;
    public int maxCountPerRow;
    public int perRowHight;
    public int PerWordWidth;
    public Text inputText;
    public Image playerHead;
    public Image NpcName;
    public Image talkBackground;
    public int wideth = 640;
    public int prefabHeightAdded;
    // Use this for initialization
    void Start () {

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void InputStr(string data, Sprite headSprite, Sprite nameSprite) {
        if (headSprite != null)
            playerHead.sprite = headSprite;
        if (nameSprite != null)
            NpcName.sprite = nameSprite;
        int widthAdd = testLeft + testRight;
        int heightAdd = testTop + testBottom;
        int prefabHeight = prefabHeightAdded + heightAdd;
        RectTransform tran = this.GetComponent<RectTransform>();
        if (data.Length < maxCountPerRow) { 
            prefabHeight += perRowHight;
            talkBackground.GetComponent<RectTransform>().sizeDelta = new Vector2(data.Length * PerWordWidth + widthAdd, perRowHight+ heightAdd);
        }
        else {
            int row = (data.Length - 1) / maxCountPerRow + 1;
            prefabHeight += perRowHight * row;
            talkBackground.GetComponent<RectTransform>().sizeDelta = new Vector2(maxCountPerRow * PerWordWidth + widthAdd, perRowHight*row + heightAdd);
        }
        this.GetComponent<RectTransform>().sizeDelta = new Vector2(wideth, prefabHeight);
        inputText.text = data;
    }

    public void InputStr(string data) {
        InputStr(data, null, null);
    }
}
