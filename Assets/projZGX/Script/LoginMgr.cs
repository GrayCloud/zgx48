﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoginMgr : MonoBehaviour {
    public Sprite[] headList;
    public Sprite[] talkHeadList;
    public Image headImage;
    public Text inputTest;
    public Image LoadingImg;
    public Button LoginButton;

    private int maxCount;
    private int currentIndex;

    private void Awake()
    {
        currentIndex = 0;
        maxCount = headList.Length;
        headImage.sprite = headList[currentIndex];
    }
    // Use this for initialization
    void Start () {
        LoadingImg.gameObject.SetActive(false);
    }
	
	// Update is called once per frame
	void Update () {
        if (inputTest.text.Trim() == "")
            LoginButton.interactable = false;
        else
            LoginButton.interactable = true;
    }

    public void GoLeft() {
        currentIndex--;
        currentIndex = currentIndex >= 0 ? currentIndex : currentIndex + maxCount;
        Debug.Log("GoLeft:" + currentIndex);
        headImage.sprite = headList[currentIndex];
    }

    public void GoRight()
    {
        currentIndex++;
        currentIndex = currentIndex >= maxCount ? currentIndex - maxCount : currentIndex;
        Debug.Log("GoRight:" + currentIndex);
        headImage.sprite = headList[currentIndex];
    }

    public void Login() {
        string playName = inputTest.text.Trim();
        LoadingImg.gameObject.SetActive(true);
        StoryMgr.GetInstance().Init(playName, talkHeadList[currentIndex]);
        //todo  remember name
        this.gameObject.SetActive(false);
    }
}
