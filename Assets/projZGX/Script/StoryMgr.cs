﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using SimpleJSON;
using System;
using UnityEngine.UI;

[System.Serializable]
public struct NPCInfo {
    public string name;
    public WXScrollMgr scrolMgr;
    public HeadMgr headMgr;
}

public class StoryMgr : MonoBehaviour {
    const string configName = "config.json";
    const string beginStory = "dialog0";
    const int defaultDelayTime = 3000;
    private string userChooseFile;

    internal void ChooseNpcByIndex(int index)
    {
        NPCInfo info = npcInfos[index];
        info.scrolMgr.setActived(true);
        info.headMgr.SetActive(true);
        for (int i = 0; i < npcInfos.Length; i++)
        {
            if (i != index) {
                info = npcInfos[i];
                info.scrolMgr.setActived(false);
                info.headMgr.SetActive(false);
            }
        }
    }

    internal void ReBegin()
    {
        AudioMgr.GetInstance().ReturnLogin();
        for (int i = 0; i < npcInfos.Length; i++)
        {
            NPCInfo info = npcInfos[i];
            info.scrolMgr.ClearAllMsg();
        }
        loginUI.SetActive(true);
    }

    private JSONArray chooseArray;
    private JSONClass totleConfigClass;
    private int delayTime = 0;
    private float passedTime = 0.0f;

    private JSONArray currentDialogs;
    private JSONClass waitingChoose;
    private string currentSender;
    private string currentDialogName;
    private int currentIndex;

    private bool updateFlag = false;
    private Dictionary<string, WXScrollMgr> npcInfoDict = new Dictionary<string, WXScrollMgr>();

    private static StoryMgr instance;
    public NPCInfo[] npcInfos;
    public Image loadingImg;
    public GameObject loginUI;

    private void Awake()
    {
        for (int i = 0; i < npcInfos.Length; i++)
        {
            NPCInfo info = npcInfos[i];
            npcInfoDict.Add(info.name, info.scrolMgr);
        }
        instance = this;
    }

    internal void MakeChoose(int v)
    {
        string nextChosse = waitingChoose.GetValueByKey("next")[v];
        string info = waitingChoose.GetValueByKey("info")[v];
        npcInfoDict[currentSender].AddSelfMsg(info);
        BeginNewStory(nextChosse);
    }

    internal void Quit()
    {
        string fullName = Path.Combine(Application.persistentDataPath, userChooseFile);
        if(File.Exists(fullName))
            File.Delete(fullName);
        //UnityEditor.EditorApplication.isPlaying = false;
    }

    public static StoryMgr GetInstance() {
        return instance;
    }

    // Use this for initialization
    void Start () {

	}
	
	// Update is called once per frame
	void Update () {
        if (!updateFlag)
            return;
        if (delayTime > 0 && delayTime > passedTime) {
            passedTime += Time.deltaTime * 1000;
            return;
        }
        if (currentIndex >= currentDialogs.Count)
            return;
        JSONClass dialogClass = currentDialogs[currentIndex] as JSONClass;
        if (delayTime > 0)
        {
            currentIndex++;
            handleOneNewMsg(npcInfoDict[currentSender], dialogClass);
            delayTime = 0;

        }
        else {
            delayTime = dialogClass.GetValueByKey("delay") == null ? defaultDelayTime : int.Parse(dialogClass.GetValueByKey("delay"));
            passedTime = 0.0f;
            npcInfoDict[currentSender].AddWaitingFlag();
        }
	}

    public void Init(string name, Sprite playerHead) {
        userChooseFile = name + ".txt";
        AudioMgr.GetInstance().GoGaming();
        InitScrollMgrs(name, playerHead);
        LoadHistoryChoose();
        LoadConfig();
        HandleHistoryMsgs();
        int cuttenIndex = InitBusyFlag();
        ChooseNpcByIndex(cuttenIndex);
        loadingImg.gameObject.SetActive(false);
    }

    private int InitBusyFlag()
    {
        int index = 0;
        for (int i = 0; i < npcInfos.Length; i++)
        {
            NPCInfo info = npcInfos[i];
            info.scrolMgr.setBusyFlag(info.name != currentSender);
            if (info.name == currentSender)
                index = i;

        }
        return index;
    }

    private void InitScrollMgrs(string name, Sprite playerHead)
    {
        for (int i = 0; i < npcInfos.Length; i++)
        {
            NPCInfo info = npcInfos[i];
            info.scrolMgr.Init(name, playerHead);
        }
    }


    void handleOneNewMsg(WXScrollMgr scrollMsg, JSONClass dialogClass) {
        string type = dialogClass.GetValueByKey("type");
        if (type == null || type == "text") {
            scrollMsg.AddTextMsg(dialogClass.GetValueByKey("info"), dialogClass.GetValueByKey("time"), true);
            return;
        }
        if (type == "image")
        {
            scrollMsg.AddImgagMsg(dialogClass.GetValueByKey("info"), dialogClass.GetValueByKey("time"), true);
        }
        else if (type == "choose") {
            JSONArray anniuArray = dialogClass.GetValueByKey("anniu") as JSONArray;
            scrollMsg.AddChooseMsg(anniuArray[0], anniuArray[1]);
            EndCurrentStory(currentDialogName);
        }
    }

    void HandleOneOldStory(string dialog, int nextIndex) {
        JSONClass oneStoryClass = totleConfigClass[dialog] as JSONClass;
        currentSender = oneStoryClass["sender"];
        if (!npcInfoDict.ContainsKey(currentSender))
            return;
        WXScrollMgr scrollMsg = npcInfoDict[currentSender];
        JSONArray dialogs = oneStoryClass["dialogArray"] as JSONArray;
        for (int i = 0; i < dialogs.Count; i++) {
            JSONClass dialogClass = dialogs[i] as JSONClass;
            string type = dialogClass.GetValueByKey("type");
            if (type == null || type == "text")
            {
                scrollMsg.AddTextMsg(dialogClass.GetValueByKey("info"), dialogClass.GetValueByKey("time"), false);
                continue;
            }

            if (type == "image")
            {
                scrollMsg.AddImgagMsg(dialogClass.GetValueByKey("info"), dialogClass.GetValueByKey("time"), false);
            }
            else if (type == "choose")
            {
                if (nextIndex >= chooseArray.Count)
                {
                    JSONArray anniuArray = dialogClass.GetValueByKey("anniu") as JSONArray;
                    scrollMsg.AddChooseMsg(anniuArray[0], anniuArray[1]);
                    waitingChoose = dialogs[dialogs.Count - 1] as JSONClass;
                    updateFlag = false;
                }
                else {
                    JSONArray infoArray = dialogClass.GetValueByKey("info") as JSONArray;
                    JSONArray nextArray = dialogClass.GetValueByKey("next") as JSONArray;
                    string nextStory = chooseArray[nextIndex];
                    for (int j = 0; j < nextArray.Count; j++) {
                        string temp = nextArray[j];
                        if (temp == nextStory) {
                            scrollMsg.AddSelfMsg(infoArray[j]);
                            break;
                        }
                    }
                }
            }
        }
    }

    void LoadHistoryChoose() {
        string fullName = Path.Combine(Application.persistentDataPath, userChooseFile);
        if (!File.Exists(fullName))
        {
            chooseArray = new JSONArray();
            return;
        }

        FileStream fs = File.OpenRead(fullName);
        using (StreamReader sr = new StreamReader(fs))
        {
            string s = sr.ReadToEnd();
            chooseArray = JSONClass.Parse(s) as JSONArray;
        }
        fs.Close();
    }

    void LoadConfig() {
        string fullName = Path.Combine(Application.streamingAssetsPath, configName);
        if (!File.Exists(fullName))
        {
            throw new Exception(fullName + " is missing!!!");
        }

        FileStream fs = File.OpenRead(fullName);
        using (StreamReader sr = new StreamReader(fs))
        {
            string s = sr.ReadToEnd();
            totleConfigClass = JSONClass.Parse(s) as JSONClass;
        }
        fs.Close();
    }

    void HandleHistoryMsgs() {
        if (chooseArray.Count > 0)
        {
            string dialog = "";
            for (int i = 0; i < chooseArray.Count; i++) {
                dialog = chooseArray[i];
                HandleOneOldStory(dialog, i + 1);
            }
        }
        else {
            BeginNewStory(beginStory);
        }
    }

    void BeginNewStory(string dialog) {
        currentDialogName = dialog;
        JSONClass currentDialogClass = totleConfigClass[dialog] as JSONClass;
        string newSender = currentDialogClass.GetValueByKey("sender");
        if (newSender == "" || newSender == null)
            newSender = currentSender;
        if (currentSender != newSender) {
            currentSender = newSender;
            InitBusyFlag();
        }
        currentDialogs = currentDialogClass["dialogArray"] as JSONArray;
        currentIndex = 0;
        updateFlag = true;
        AddChoose(dialog);
    }

    void EndCurrentStory(string dialog)
    {
        waitingChoose = currentDialogs[currentDialogs.Count - 1] as JSONClass;
        updateFlag = false;
    }

    void AddChoose(string dialog) {
        chooseArray[chooseArray.Count] = dialog;
        string choose = chooseArray.ToString();
        string path = Path.Combine(Application.persistentDataPath, userChooseFile);
        try {
            FileStream fs = File.Open(path, FileMode.Create);
            using (StreamWriter sw = new StreamWriter(fs))
            {
                sw.Write(choose);
            }
            fs.Close();
        }
        catch (Exception e) {
            Debug.Log(string.Format("Add Choose Error:{0};string;{1}", e.ToString(), choose));
        }
    }
}
