﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdateMgrTest : MonoBehaviour {
    public AdapatableText text1;
    public AdapatableText text2;
    public AdapatableText text3;
    public WXScrollMgr scrollMsg;
    public StoryMgr storyMgr;
    public Sprite head;

    // Use this for initialization
    void Start1 () {
        string test1 = "长度很短";
        string test2 = "长度很长长度很长长度很长长度很长长度很长长度很长长度很长长度很长长度很长长度很长长度很长长度很长长度很长长度很长长度很长长度很长长度很长长度很长长度很长长度很长长度很长";
        string test3 = "引文数字afdgarga1111111111111asfdga222";
        text1.InputStr(test1);
        text2.InputStr(test2);
        text3.InputStr(test3);
    }

    private void Start()
    {
        string test1 = "长度很短";
        string test2 = "长度很长长度很长长度很长长度很长长度很长长度很长长度很长长度很长长度很长长度很长长度很长长度很长长度很长长度很长长度很长长度很长长度很长长度很长长度很长长度很长长度很长";
        string test3 = "引文数字afdgarga1111111111111asfdga222";
        StoryMgr.GetInstance().Init("test1", head);
        //scrollMsg.AddTimeMsg("22:40", true);
        //scrollMsg.AddImgagMsg("img2", true);
        //scrollMsg.AddTimeMsg("22:40", true);
        //scrollMsg.AddImgagMsg("img2", true);
        //scrollMsg.AddTimeMsg("事件很晚", true);
        //scrollMsg.AddNewMsg(test3, true);
        //scrollMsg.AddTimeMsg("22:40", true);
        //scrollMsg.AddNewMsg(test3, true);
        //scrollMsg.AddSelfMsg(test2);
        //scrollMsg.AddSelfMsg(test2);
    }

    public void addNewMsg() {
        string str = "新消息啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊";
        int rand = Random.Range(1, 10);
        scrollMsg.AddNewMsg(str + rand, true);
    }

    public void ChooseOne()
    {
        StoryMgr.GetInstance().MakeChoose(0);
    }

    public void ChooseSecond()
    {
        StoryMgr.GetInstance().MakeChoose(1);
    }

    public void Quit()
    {
        StoryMgr.GetInstance().Quit();
    }
}
