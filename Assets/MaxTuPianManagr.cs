﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MaxTuPianManagr : MonoBehaviour {
    public Image realImg;
    public static MaxTuPianManagr instance;
    private void Awake()
    {
        instance = this;
    }
    // Use this for initialization
    void Start () {
        this.gameObject.SetActive(false);
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public static MaxTuPianManagr GetInstance() {
        return instance;
    }

    public void ShowMax(Sprite tupian) {
        realImg.sprite = tupian;
        realImg.SetNativeSize();
        this.gameObject.SetActive(true);
    }
    public void OnClick() {
        this.gameObject.SetActive(false);
    }
}
