﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PictureMgr : MonoBehaviour {
    public Image touXiang;
    public Image tuPian;
    public Image npcName;
    public int top;
    public int width = 640;
    private Sprite tupianSprite;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void SetTuPian(string tupianName, Sprite touxiang, Sprite name) {
        touXiang.sprite = touxiang;
        tupianSprite = Resources.Load<Sprite>(tupianName);
        tuPian.sprite = tupianSprite;
        if (tupianSprite != null) {
            float height = tupianSprite.textureRect.height;
            float width = tupianSprite.textureRect.width;
            if (width > 400 || height > 400)
            {
                if (width > height)
                {
                    tuPian.GetComponent<RectTransform>().sizeDelta = new Vector2(400, 400 * height / width);
                }
                tuPian.GetComponent<RectTransform>().sizeDelta = new Vector2(400 * width / height, 400);
            }
            else {
                tuPian.SetNativeSize();
            }
        }

        //tuPian.SetNativeSize();
        Vector2 v2 = tuPian.GetComponent<RectTransform>().sizeDelta;
        this.GetComponent<RectTransform>().sizeDelta = new Vector2(width, v2.y + top);
        npcName.sprite = name;
    }


    public void onPictureClick() {
        if(tupianSprite != null)
            MaxTuPianManagr.GetInstance().ShowMax(tupianSprite);
    }
}
