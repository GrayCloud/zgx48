﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettringMgr : MonoBehaviour {
    public GameObject settingUI;
    private bool isShow = false;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnSetting() {
        isShow = !isShow;
        settingUI.SetActive(isShow);
    }

    public void OnReturn() {
        isShow = false;
        settingUI.SetActive(isShow);
    }

    public void OnAgian() {
        OnReturn();
        StoryMgr.GetInstance().ReBegin();
    }
}
