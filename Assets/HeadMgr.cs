﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HeadMgr : MonoBehaviour
{
    public Image headImg;
    public GameObject msgCount;
    public Sprite activeHead;
    public Sprite unActiveHead;
    public WXScrollMgr scrollMgr;
    public int index;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (scrollMgr.GetUnReadMsgCount() > 0)
        {
            msgCount.GetComponentInChildren<Text>().text = scrollMgr.GetUnReadMsgCount().ToString();
            msgCount.gameObject.SetActive(true);
        }
        else
            msgCount.gameObject.SetActive(false);
    }

    public void OnClick()
    {
        StoryMgr.GetInstance().ChooseNpcByIndex(index);
    }

    public void SetActive(bool isActive)
    {
        Debug.Log(string.Format("index:{0},isActive:{1}", index, isActive));
        if (isActive)
        {
            headImg.SetNativeSize();
            headImg.sprite = activeHead;
            this.GetComponent<Button>().interactable = false;
            //this.GetComponent<Button>().enabled = false;
        }
        else
        {
            headImg.sprite = unActiveHead;
            headImg.SetNativeSize();
            this.GetComponent<Button>().interactable = true;
            //this.GetComponent<Button>().enabled = true;
        }
    }
}

