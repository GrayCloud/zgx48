﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioMgr : MonoBehaviour {
    public AudioClip[] otherClip;
    public AudioClip loginClip;
    public AudioSource audioS;
    static AudioMgr instance;
    private float nextTime = 0.0f;
    private bool gamingFlag = false;
    private int index = 0;

    private void Awake()
    {
        instance = this;
    }

    public static AudioMgr GetInstance() {
        return instance;
    }
    // Use this for initialization
    void Start () {
        ReturnLogin();
    }

    public void ReturnLogin() {
        gamingFlag = false;
        audioS.clip = loginClip;
        audioS.loop = true;
        audioS.Play();
    }

    public void GoGaming()
    {
        gamingFlag = true;
        audioS.clip = otherClip[index];
        index++;
        if (index == otherClip.Length)
            index = 0;
        audioS.loop = false;
        audioS.Play();
        nextTime = audioS.clip.length;

    }

    // Update is called once per frame
    void Update () {
        if (!gamingFlag)
            return;
        nextTime -= Time.deltaTime;
        if (nextTime < 0)
            GoGaming();
    }
}
